package com.example.fitnesstracker;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;

public class WorkoutPlanController {
    @FXML
    private Label lblUsername;

    @FXML
    private TableView<StrengthExercise> absTable;
    @FXML
    private TableColumn<StrengthExercise, String> absExerciseColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> absSetsColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> absRepsColumn;
    @FXML
    private TableColumn<StrengthExercise, Double> absWeightColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> absRestTimeColumn;

    @FXML
    private TableView<StrengthExercise> chestTable;
    @FXML
    private TableColumn<StrengthExercise, String> chestExerciseColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> chestSetsColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> chestRepsColumn;
    @FXML
    private TableColumn<StrengthExercise, Double> chestWeightColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> chestRestTimeColumn;

    @FXML
    private TableView<StrengthExercise> backTable;
    @FXML
    private TableColumn<StrengthExercise, String> backExerciseColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> backSetsColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> backRepsColumn;
    @FXML
    private TableColumn<StrengthExercise, Double> backWeightColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> backRestTimeColumn;

    @FXML
    private TableView<StrengthExercise> armsTable;
    @FXML
    private TableColumn<StrengthExercise, String> armsExerciseColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> armsSetsColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> armsRepsColumn;
    @FXML
    private TableColumn<StrengthExercise, Double> armsWeightColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> armsRestTimeColumn;

    @FXML
    private TableView<StrengthExercise> legsTable;
    @FXML
    private TableColumn<StrengthExercise, String> legsExerciseColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> legsSetsColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> legsRepsColumn;
    @FXML
    private TableColumn<StrengthExercise, Double> legsWeightColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> legsRestTimeColumn;

    @FXML
    private TableView<CardioExercise> cardioTable;
    @FXML
    private TableColumn<CardioExercise, String> cardioExerciseColumn;
    @FXML
    private TableColumn<CardioExercise, Double> cardioDurationColumn;
    @FXML
    private TableColumn<CardioExercise, Double> cardioDistanceColumn;
    @FXML
    private TableColumn<CardioExercise, Integer> cardioRestTimeColumn;

    private ObservableList<StrengthExercise> absExercises;
    private ObservableList<StrengthExercise> chestExercises;
    private ObservableList<StrengthExercise> backExercises;
    private ObservableList<StrengthExercise> armsExercises;
    private ObservableList<StrengthExercise> legsExercises;
    private ObservableList<CardioExercise> cardioExercises;

    public void initialize(){
        absExercises = FXCollections.observableArrayList();
        chestExercises = FXCollections.observableArrayList();
        backExercises = FXCollections.observableArrayList();
        armsExercises = FXCollections.observableArrayList();
        legsExercises = FXCollections.observableArrayList();
        cardioExercises = FXCollections.observableArrayList();

        absExerciseColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        absSetsColumn.setCellValueFactory(new PropertyValueFactory<>("sets"));
        absRepsColumn.setCellValueFactory(new PropertyValueFactory<>("reps"));
        absWeightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        absRestTimeColumn.setCellValueFactory(new PropertyValueFactory<>("restTime"));

        chestExerciseColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        chestSetsColumn.setCellValueFactory(new PropertyValueFactory<>("sets"));
        chestRepsColumn.setCellValueFactory(new PropertyValueFactory<>("reps"));
        chestWeightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        chestRestTimeColumn.setCellValueFactory(new PropertyValueFactory<>("restTime"));

        backExerciseColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        backSetsColumn.setCellValueFactory(new PropertyValueFactory<>("sets"));
        backRepsColumn.setCellValueFactory(new PropertyValueFactory<>("reps"));
        backWeightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        backRestTimeColumn.setCellValueFactory(new PropertyValueFactory<>("restTime"));

        armsExerciseColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        armsSetsColumn.setCellValueFactory(new PropertyValueFactory<>("sets"));
        armsRepsColumn.setCellValueFactory(new PropertyValueFactory<>("reps"));
        armsWeightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        armsRestTimeColumn.setCellValueFactory(new PropertyValueFactory<>("restTime"));

        legsExerciseColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        legsSetsColumn.setCellValueFactory(new PropertyValueFactory<>("sets"));
        legsRepsColumn.setCellValueFactory(new PropertyValueFactory<>("reps"));
        legsWeightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        legsRestTimeColumn.setCellValueFactory(new PropertyValueFactory<>("restTime"));

        cardioExerciseColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        cardioDurationColumn.setCellValueFactory(new PropertyValueFactory<>("duration"));
        cardioDistanceColumn.setCellValueFactory(new PropertyValueFactory<>("distance"));
        cardioRestTimeColumn.setCellValueFactory(new PropertyValueFactory<>("restTime"));

        loadExercises();

        loginUsername();
    }

    private void loadExercises() {
        try {
            absExercises = ExerciseSerializer.readStrengthExercises("abs_exercises.json");
            chestExercises = ExerciseSerializer.readStrengthExercises("chest_exercises.json");
            backExercises = ExerciseSerializer.readStrengthExercises("back_exercises.json");
            armsExercises = ExerciseSerializer.readStrengthExercises("arms_exercises.json");
            legsExercises = ExerciseSerializer.readStrengthExercises("legs_exercises.json");
            cardioExercises = ExerciseSerializer.readCardioExercises();

            absTable.setItems(absExercises);
            chestTable.setItems(chestExercises);
            backTable.setItems(backExercises);
            armsTable.setItems(armsExercises);
            legsTable.setItems(legsExercises);
            cardioTable.setItems(cardioExercises);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void loginUsername() {
        String username = LoginSingleton.getInstance().getLoginUsername();
        lblUsername.setText("Welcome, " + username);
    }

    @FXML
    public void ChangeToScene(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("ExerciseList.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1024, 860);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("Exercise List");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void ChangeToScene2(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("login.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1024, 860);
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("Login Page");
        stage.setScene(scene);
        stage.show();
    }
}
