package com.example.fitnesstracker;

import javafx.event.ActionEvent;

import java.io.IOException;

public interface IChangeScenes {

    void ChangeToScene(ActionEvent event) throws IOException;
}
