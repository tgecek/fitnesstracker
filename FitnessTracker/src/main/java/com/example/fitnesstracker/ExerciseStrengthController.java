package com.example.fitnesstracker;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.IOException;

public class ExerciseStrengthController {
    @FXML
    private TableView<StrengthExercise> exerciseTable;
    @FXML
    private TableColumn<StrengthExercise, String> nameColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> setsColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> repsColumn;
    @FXML
    private TableColumn<StrengthExercise, Double> weightColumn;
    @FXML
    private TableColumn<StrengthExercise, Integer> restTimeColumn;

    @FXML
    private TextField txtStrengthName, txtSets, txtReps, txtWeight, txtRestTime;
    @FXML
    private HBox strengthInputs;
    private ObservableList<StrengthExercise> exercises;
    private String jsonFile;

    public void initialize(String jsonFile) {
        this.jsonFile = jsonFile;
        exercises = FXCollections.observableArrayList();

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        setsColumn.setCellValueFactory(new PropertyValueFactory<>("sets"));
        repsColumn.setCellValueFactory(new PropertyValueFactory<>("reps"));
        weightColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        restTimeColumn.setCellValueFactory(new PropertyValueFactory<>("restTime"));

        exerciseTable.setItems(exercises);

        loadExercises();
    }

    private void loadExercises() {
        try {
            ObservableList<StrengthExercise> loadedExercises = ExerciseSerializer.readStrengthExercises(jsonFile);
            exercises.addAll(loadedExercises);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void addExercise(ActionEvent event) {
        StrengthExercise newExercise = new StrengthExercise(
                txtStrengthName.getText(),
                Integer.parseInt(txtSets.getText()),
                Integer.parseInt(txtReps.getText()),
                Double.parseDouble(txtWeight.getText()),
                Integer.parseInt(txtRestTime.getText())
        );
        exercises.add(newExercise);
        saveExercises();
    }

    @FXML
    private void editExercise(ActionEvent event) {
        StrengthExercise selectedExercise = exerciseTable.getSelectionModel().getSelectedItem();
        if (selectedExercise != null) {
            selectedExercise.setName(txtStrengthName.getText());
            selectedExercise.setSets(Integer.parseInt(txtSets.getText()));
            selectedExercise.setReps(Integer.parseInt(txtReps.getText()));
            selectedExercise.setWeight(Double.parseDouble(txtWeight.getText()));
            selectedExercise.setRestTime(Integer.parseInt(txtRestTime.getText()));
            exerciseTable.refresh();
            saveExercises();
        }
    }

    @FXML
    private void deleteExercise(ActionEvent event) {
        StrengthExercise selectedExercise = exerciseTable.getSelectionModel().getSelectedItem();
        if (selectedExercise != null) {
            exercises.remove(selectedExercise);
            saveExercises();
        }
    }

    private void saveExercises() {
        try {
            ExerciseSerializer.writeStrengthExercises(exercises, jsonFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void ReturnToScene(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("ExerciseList.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1024, 860);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("workoutPlan");
        stage.setScene(scene);
        stage.show();
    }
}
