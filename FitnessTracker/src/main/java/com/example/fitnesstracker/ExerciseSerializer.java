package com.example.fitnesstracker;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

public class ExerciseSerializer {
    private static final Gson gson = new GsonBuilder()
            .registerTypeAdapter(Exercise.class, new ExerciseDeserializer())
            .create();

    public static ObservableList<StrengthExercise> readStrengthExercises(String filePath) throws IOException {
        try (FileReader reader = new FileReader(filePath)) {
            Type listType = new TypeToken<List<StrengthExercise>>(){}.getType();
            List<StrengthExercise> list = gson.fromJson(reader, listType);
            return FXCollections.observableArrayList(list);
        }
    }

    public static void writeStrengthExercises(ObservableList<StrengthExercise> exercises, String filePath) throws IOException {
        try (FileWriter writer = new FileWriter(filePath)) {
            gson.toJson(exercises, writer);
        }
    }

    public static ObservableList<CardioExercise> readCardioExercises() throws IOException {
        try (FileReader reader = new FileReader("cardio_exercises.json")) {
            Type listType = new TypeToken<List<CardioExercise>>(){}.getType();
            List<CardioExercise> list = gson.fromJson(reader, listType);
            return FXCollections.observableArrayList(list);
        }
    }

    public static void writeCardioExercises(ObservableList<CardioExercise> exercises) throws IOException {
        try (FileWriter writer = new FileWriter("cardio_exercises.json")) {
            gson.toJson(exercises, writer);
        }
    }
}
