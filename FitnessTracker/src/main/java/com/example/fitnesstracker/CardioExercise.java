package com.example.fitnesstracker;

public class CardioExercise extends Exercise {
    private double duration;
    private double distance;

    public CardioExercise(String name, double duration, double distance, int restTime) {
        super(name, restTime);
        this.duration = duration;
        this.distance = distance;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    @Override
    public String toString() {
        return "CardioExercise{" +
                "name='" + name + '\'' +
                ", duration=" + duration +
                ", distance=" + distance +
                ", restTime=" + restTime +
                '}';
    }
}
