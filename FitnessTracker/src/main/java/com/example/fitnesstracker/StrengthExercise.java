package com.example.fitnesstracker;

public class StrengthExercise extends Exercise {
    private int sets;
    private int reps;
    private double weight;

    public StrengthExercise(String name, int sets, int reps, double weight, int restTime) {
        super(name, restTime);
        this.sets = sets;
        this.reps = reps;
        this.weight = weight;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "StrengthExercise{" +
                "name='" + name + '\'' +
                ", sets=" + sets +
                ", reps=" + reps +
                ", weight=" + weight +
                ", restTime=" + restTime +
                '}';
    }
}
