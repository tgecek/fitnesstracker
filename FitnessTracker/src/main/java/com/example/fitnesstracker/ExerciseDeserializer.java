package com.example.fitnesstracker;

import com.google.gson.*;

import java.lang.reflect.Type;

public class ExerciseDeserializer implements JsonDeserializer<Exercise> {

    @Override
    public Exercise deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        String name = jsonObject.get("name").getAsString();
        int restTime = jsonObject.get("restTime").getAsInt();

        if (jsonObject.has("duration") && jsonObject.has("distance")) {
            double duration = jsonObject.get("duration").getAsDouble();
            double distance = jsonObject.get("distance").getAsDouble();
            return new CardioExercise(name, duration, distance, restTime);
        } else {
            int sets = jsonObject.get("sets").getAsInt();
            int reps = jsonObject.get("reps").getAsInt();
            double weight = jsonObject.get("weight").getAsDouble();
            return new StrengthExercise(name, sets, reps, weight, restTime);
        }
    }
}
