package com.example.fitnesstracker;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class ExerciseListController {




    @FXML
    public void absButtonClick(ActionEvent event) throws IOException {
        loadStrengthScene(event, "abs_exercises.json");
    }

    @FXML
    public void chestButtonClick(ActionEvent event) throws IOException {
        loadStrengthScene(event, "chest_exercises.json");
    }

    @FXML
    public void backButtonClick(ActionEvent event) throws IOException {
        loadStrengthScene(event, "back_exercises.json");
    }

    @FXML
    public void armsButtonClick(ActionEvent event) throws IOException {
        loadStrengthScene(event, "arms_exercises.json");
    }

    @FXML
    public void legsButtonClick(ActionEvent event) throws IOException {
        loadStrengthScene(event, "legs_exercises.json");
    }

    @FXML
    public void cardioButtonClick(ActionEvent event) throws IOException {
        ChangeToScene(event, "Cardio");
    }

    private void ChangeToScene(ActionEvent event, String category) throws IOException {
        String fxmlFile;
        if (category.equals("Cardio")) {
            fxmlFile = "exerciseCardio.fxml";
        } else {
            fxmlFile = "exerciseStrength.fxml";
        }

        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource(fxmlFile));
        Scene scene = new Scene(fxmlLoader.load(), 1024, 860);

        if (category.equals("Cardio")) {
            ExerciseCardioController controller = fxmlLoader.getController();
            controller.initialize();
        } else {
            ExerciseStrengthController controller = fxmlLoader.getController();
            controller.initialize("Chest");
        }

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("Exercise Details");
        stage.setScene(scene);
        stage.show();
    }

    private void loadStrengthScene(ActionEvent event, String jsonFile) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("exerciseStrength.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1024, 860);

        ExerciseStrengthController controller = fxmlLoader.getController();
        controller.initialize(jsonFile);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("Exercise Details");
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void ReturnToScene(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("workoutPlan.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 1024, 860);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("workoutPlan");
        stage.setScene(scene);
        stage.show();
    }
}
