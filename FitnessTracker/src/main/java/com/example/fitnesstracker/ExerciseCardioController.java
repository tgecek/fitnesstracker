package com.example.fitnesstracker;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.List;

public class ExerciseCardioController {

    @FXML
    private TableView<CardioExercise> cardioTable;
    @FXML
    private TableColumn<CardioExercise, String> nameColumn;
    @FXML
    private TableColumn<CardioExercise, Double> durationColumn;
    @FXML
    private TableColumn<CardioExercise, Double> distanceColumn;
    @FXML
    private TableColumn<CardioExercise, Integer> restTimeColumn;

    @FXML
    private TextField txtCardioName, txtDuration, txtDistance, txtRestTime;

    private ObservableList<CardioExercise> cardioExercises;

    public void initialize() {
        cardioExercises = FXCollections.observableArrayList();

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        durationColumn.setCellValueFactory(new PropertyValueFactory<>("duration"));
        distanceColumn.setCellValueFactory(new PropertyValueFactory<>("distance"));
        restTimeColumn.setCellValueFactory(new PropertyValueFactory<>("restTime"));

        cardioTable.setItems(cardioExercises);

        loadExercises();
    }

    private void loadExercises() {
        try {
            List<CardioExercise> loadedExercises = ExerciseSerializer.readCardioExercises();
            cardioExercises.addAll(loadedExercises);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void addCardioExercise(ActionEvent event) {
        CardioExercise newCardioExercise = new CardioExercise(
                txtCardioName.getText(),
                Double.parseDouble(txtDuration.getText()),
                Double.parseDouble(txtDistance.getText()),
                Integer.parseInt(txtRestTime.getText())
        );
        cardioExercises.add(newCardioExercise);
        saveExercises();
    }

    @FXML
    private void editCardioExercise(ActionEvent event) {
        CardioExercise selectedCardioExercise = cardioTable.getSelectionModel().getSelectedItem();
        if (selectedCardioExercise != null) {
            selectedCardioExercise.setName(txtCardioName.getText());
            selectedCardioExercise.setDuration(Double.parseDouble(txtDuration.getText()));
            selectedCardioExercise.setDistance(Double.parseDouble(txtDistance.getText()));
            selectedCardioExercise.setRestTime(Integer.parseInt(txtRestTime.getText()));
            cardioTable.refresh();
            saveExercises();
        }
    }

    @FXML
    private void deleteCardioExercise(ActionEvent event) {
        CardioExercise selectedCardioExercise = cardioTable.getSelectionModel().getSelectedItem();
        if (selectedCardioExercise != null) {
            cardioExercises.remove(selectedCardioExercise);
            saveExercises();
        }
    }

    private void saveExercises() {
        try {
            ExerciseSerializer.writeCardioExercises(cardioExercises);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    public void ReturnToScene(ActionEvent event) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("ExerciseList.fxml"));
        Scene scene = null;
        try{
            scene = new Scene(fxmlLoader.load(), 1024, 860);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("workoutPlan");
        stage.setScene(scene);
        stage.show();
    }
}
