package com.example.fitnesstracker;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class LoginController implements IChangeScenes{
    @FXML
    private TextField inputUsername;
    @FXML
    private PasswordField inputPass;
    @FXML
    private Button btnSignIn;
    @FXML
    private Label lblIncorrect;
    private List<User> userList;

    public void userDatabase() {
        userList = new ArrayList<>();
        userList.add(new User("Tomo","123"));
        userList.add(new User("Marko","123w"));
        userList.add(new User("Dominik","koliko99"));
    }

    public LoginController(){
        userDatabase();
    }

    @FXML
    public void kriviLogin(){
        lblIncorrect.setOpacity(1.0);
    }

    @FXML
    public TextField getInputUsername(){
        return inputUsername;
    }
    @FXML
    public PasswordField getInputPassword(){
        return inputPass;
    }

    public boolean authenticator(String username, String password){
        for (User user : userList) {
            if (user.getUsername().equals(username) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    @Override
    @FXML
    public void ChangeToScene(ActionEvent event) throws IOException {
        if(authenticator(getInputUsername().getText(), getInputPassword().getText())){
            FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("workoutPlan.fxml"));
            Scene scene = null;

        LoginSingleton.getInstance().setLoginUsername(getInputUsername().getText());
        try{
            scene = new Scene(fxmlLoader.load(), 1024, 860);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle("workoutPlan");
        stage.setScene(scene);
        stage.show();
        } else {
            kriviLogin();
        }
    }

}