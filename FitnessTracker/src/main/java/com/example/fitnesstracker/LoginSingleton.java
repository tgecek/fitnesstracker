package com.example.fitnesstracker;

public class LoginSingleton {
    private static LoginSingleton Instance;
    private String loggedUsername;
    private LoginSingleton(){
    }

    public static LoginSingleton getInstance() {
        if(Instance == null){
            Instance = new LoginSingleton();
        }
        return Instance;
    }

    public void setLoginUsername(String username){
        this.loggedUsername = username;
    }

    public String getLoginUsername(){
        return loggedUsername;
    }
}
