package com.example.fitnesstracker;

public abstract class Exercise {
    protected String name;
    protected int restTime;

    public Exercise(String name, int restTime) {
        this.name = name;
        this.restTime = restTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRestTime() {
        return restTime;
    }

    public void setRestTime(int restTime) {
        this.restTime = restTime;
    }
}
