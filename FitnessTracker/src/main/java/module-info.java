module com.example.fitnesstracker {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.google.gson;

    opens com.example.fitnesstracker to com.google.gson, javafx.fxml;
    exports com.example.fitnesstracker;
}